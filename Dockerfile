FROM maven:3.8.4-openjdk-17
COPY . .
RUN mvn clean install
ENTRYPOINT ["java","-jar","./target/beta-0.0.1-SNAPSHOT.jar"]

